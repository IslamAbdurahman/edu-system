<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $request->validate([
                'phone' => 'required|exists:users,phone|max:50',
                'password' => 'required|min:4'
            ]);


            $user = \App\Models\User::where('phone', $request->phone)->first();
            if ($user && $user->password == $request->password) {
                $authToken = $user->createToken('authToken')->plainTextToken;
                $user->token = $authToken;

                return response()->json([
                    'status' => true,
                    'data' => $user,
                    'message' => 'ok',
                ]);
            } else {
                return response()->json([
                    'status' => false,
                    'data' => '',
                    'message' => 'Raqam yoki parol xato',
                ]);
            }

        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'data' => $exception->getMessage(),
                'message' => 'Raqam yoki parol xato',
            ]);
        }
    }

    public function user(Request $request){
        try {

            $request->user();

            return response()->json([
                'status' => true,
                'data' => $request->user(),
                'message' => 'ok'
            ]);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'data' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }

}
